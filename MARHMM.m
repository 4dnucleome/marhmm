function [trans,emission,pi,trans2,emission2,pi2,LOGP,ALFA1,ALFA2]=MARHMM(data,level,a,pi,iteration,alfa1,alfa2)
M=length(level(1,:));
for i=1:M
    b(:,:,i)=input('enter dependence matrix b :');
end
e=length(data(:,1));
T=length(data(1,:));
n=length(a(1,:));
LOGP=[];
PAR1=[];
PAR2=[];
for i=1:e
    data2(i,:)=rot90(data(i,:),2);
end
hh=[];
hh2=[];
yy=[];
yy2=[];
vvv=[];
vvv2=[];
LLL=[];
LLL2=[];
Lq=[];
Lq2=[];
vq=[];
vq2=[];
alf1=alfa1;
alf2=alfa2;
pe=[];
pe2=[];
for i=1:M
    b2(:,:,i)=b(:,:,i);
end
pi2=pi;
a2=a;
for r=1:e
    rdata=data(r,:);
    T=length(rdata(1,:));
    for i=1:n        %it is initilization
        alfa(1,i)=b(i,rdata(1),1)*pi(i);
    end
    for t=1:(T-1)      %recurssion
        for j=1:n
            z=0;
            for i=1:n
                z=z+a(i,j)*alfa(t,i);
            end
            for q=1:M
                if rdata(t)==level(q)
                    alfa(t+1,j)=z*b(j,rdata(t+1),q);
                end
            end
        end
    end
    p=0;
    for i=1:n         %termination
        p=p+alfa(T,i);
    end
    PAR1(1,r)=p;
end
for r=1:e
    rdata=data2(r,:);
    T=length(rdata(1,:));
    for i=1:n        %it is initilization
        alfa(1,i)=b(i,rdata(1),1)*pi(i);
    end
    for t=1:(T-1)      %recurssion
        for j=1:n
            z=0;
            for i=1:n
                z=z+a(i,j)*alfa(t,i);
            end
            for q=1:M
                if rdata(t)==level(q)
                    alfa(t+1,j)=z*b(j,rdata(t+1),q);
                end
            end
        end
    end
    p=0;
    for i=1:n         %termination
        p=p+alfa(T,i);
    end
    % p;
    PAR2(1,r)=p;
end

for qq=1:iteration
    
    for r=1:e
        pe(1,r)=(alf1*PAR1(1,r))/((alf1*PAR1(1,r))+(alf2*PAR2(1,r)));
        pe2(1,r)=(alf2*PAR2(1,r))/((alf1*PAR1(1,r))+(alf2*PAR2(1,r)));
    end
    segpe=0;
    for i=1:e
        segpe=segpe+pe(1,i);
    end
    segpe2=0;
    for i=1:e
        segpe2=segpe2+pe2(1,i);
    end
    pi1=zeros(n,1);
    pi2=zeros(n,1);
    for i=1:M
        vqq(:,:,i)=zeros(n,M);
    end
    for i=1:M
        vqq2(:,:,i)=zeros(n,M);
    end
    for i=1:M
        Lqq(:,:,i)=zeros(n,M);
    end
    for i=1:M
        Lqq2(:,:,i)=zeros(n,M);
    end
    zz=zeros(n,n);zzz=zeros(n,n);
    zz2=zeros(n,n);zzz2=zeros(n,n);
    for r=1:e
        rdata=data(r,:);
        T=length(rdata(1,:));
        for i=1:n        %it is initilization
            m(1,i)=b(i,rdata(1),1)*pi(i);
        end
        for t=1:T-1      %recurssion
            
            for j=1:n
                z=0;
                for i=1:n
                    z=z+a(i,j)*m(t,i);
                end
                for q=1:M
                    if rdata(t)==level(q)
                        m(t+1,j)=z*b(j,rdata(t+1),q);
                    end
                end
            end
        end
        
        for i=1:n        %it is initilization
            d(T,i)=1;
        end
        for t=T-1:-1:1
            for i=1:n
                for q=1:M
                    if rdata(t)==level(q)
                        z=0;
                        for j=1:n;
                            z=z+a(i,j)*d(t+1,j)*b(j,rdata(t+1),q);
                        end
                        d(t,i)=z;
                    end
                end
            end
        end
        for t=1:T
            for i=1:n
                x=0;
                for j=1:n
                    x=x+(m(t,j)*d(t,j));
                end
                
                if x~=0
                    G(t,i)=(m(t,i)*d(t,i))/(x);
                end
            end
        end
        
        for t=1:T-1
            for i=1:n
                for j=1:n
                    for q=1:M
                        if rdata(t)==level(q)
                            w=0;
                            for k=1:n
                                w=w+(m(t,k)*d(t,k));
                            end
                            
                            if w~=0
                                E(i,j,t)=(m(t,i)*a(i,j)*b(j,rdata(t+1),q)*d(t+1,j))/(w);
                            end
                        end
                    end
                end
            end
        end
        pii=zeros(n,1);
        
        for i=1:n
            pii(i,1)=G(1,i);
        end
        pi1=pi1+(pii*pe(1,r));
        for i=1:n
            for j=1:n
                h=0;y=0;
                for t=1:T-1
                    h=h+E(i,j,t);
                    y=y+G(t,i);
                end
                hh(i,j)=h;
                yy(i,j)=y;
            end
        end
        zz=zz+(hh*pe(1,r));
        zzz=zzz+(yy*pe(1,r));%  end%satr
        for q=1:M
            for j=1:n
                for k=1:M
                    L=0;
                    for t=2:T
                        if rdata(t-1)==level(q)
                            L=L+G(t,j);
                        end
                    end
                    LLL(j,k)=L;
                    v=0;
                    for  t=2:T
                        if rdata(t)==level(k) && rdata(t-1)==level(q)
                            v=v+G(t,j);
                        end
                    end
                    vvv(j,k)=v;
                end
            end
            vq(:,:,q)= vvv*pe(1,r);
            Lq(:,:,q)= LLL*pe(1,r);
        end
        for i=1:M
            vqq(:,:,i)=vqq(:,:,i)+vq(:,:,i);
            Lqq(:,:,i)=Lqq(:,:,i)+Lq(:,:,i);
        end
    end
    pi=(pi1/segpe);%end row
    for i=1:M
        b(:,:,i)=vqq(:,:,i)./Lqq(:,:,i);
    end
    a=zz./zzz;
    for r=1:e
        rdata=data2(r,:);
        T=length(rdata(1,:));
        for i=1:n        %it is initilization
            m(1,i)=b2(i,rdata(1),1)*pi2(i);
            
        end
        for t=1:T-1      %recurssion
            
            for j=1:n
                z=0;
                for i=1:n
                    z=z+a2(i,j)*m(t,i);
                end
                for q=1:M
                    if rdata(t)==level(q)
                        m(t+1,j)=z*b2(j,rdata(t+1),q);
                    end
                end
            end
        end
        
        for i=1:n        %it is initilization
            d(T,i)=1;
        end
        for t=T-1:-1:1
            for i=1:n
                for q=1:M
                    if rdata(t)==level(q)
                        z=0;
                        for j=1:n;
                            z=z+a2(i,j)*d(t+1,j)*b2(j,rdata(t+1),q);
                        end
                        d(t,i)=z;
                    end
                end
            end
        end
        for t=1:T
            for i=1:n
                x=0;
                for j=1:n
                    x=x+(m(t,j)*d(t,j));
                end
                
                if x~=0
                    G(t,i)=(m(t,i)*d(t,i))/(x);
                end
            end
        end
        
        for t=1:T-1
            for i=1:n
                for j=1:n
                    for q=1:M
                        if rdata(t)==level(q)
                            w=0;
                            for k=1:n
                                w=w+(m(t,k)*d(t,k));
                            end
                            
                            if w~=0
                                E(i,j,t)=(m(t,i)*a2(i,j)*b2(j,rdata(t+1),q)*d(t+1,j))/(w);
                            end
                        end
                    end
                end
            end
        end
        pii2=zeros(n,1);
        for i=1:n
            pii2(i,1)=G(1,i)*pe2(1,r);
        end
        pi2=pi2+pii2;
        for i=1:n
            for j=1:n
                h=0;y=0;
                for t=1:T-1
                    h=h+E(i,j,t);
                    y=y+G(t,i);
                end
                hh(i,j)=h;
                yy(i,j)=y;
            end
        end
        zz=zz+(hh*pe2(1,r));
        zzz=zzz+(yy*pe2(1,r));%  end%satr
        for q=1:M
            for j=1:n
                for k=1:M
                    L=0;
                    for t=2:T
                        if rdata(t-1)==level(q)
                            L=L+G(t,j);
                        end
                    end
                    LLL(j,k)=L;
                    v=0;
                    for  t=2:T
                        if rdata(t)==level(k) && rdata(t-1)==level(q)
                            v=v+G(t,j);
                        end
                    end
                    vvv(j,k)=v;
                end
            end
            vq(:,:,q)= vvv*pe2(1,r);
            Lq(:,:,q)= LLL*pe2(1,r);
        end
        
        for i=1:M
            vqq(:,:,i)=vqq(:,:,i)+vq(:,:,i);
            Lqq(:,:,i)=Lqq(:,:,i)+Lq(:,:,i);
        end
    end
    pi2=(pi2/segpe2);%end row
    for i=1:M
        b2(:,:,i)=vqq(:,:,i)./Lqq(:,:,i);
    end
    a2=zz./zzz;
    
    alfaa=0;
    for i=1:e
        alfaa=pe(1,i)+alfaa;
    end
    alfaaa=0;
    for i=1:e
        alfaaa=pe2(1,i)+alfaaa;
    end
    alfa1=alfaa/e;
    alfa2=alfaaa/e;
    for i=1:e
        PAR12(1,i)=(alf1*PAR1(1,i))+(alf2*PAR2(1,i));
    end
    for i=1:e
        PAR12(1,i)=log10(PAR12(1,i));
    end
    for r=1:e
        rdata=data(r,:);
        T=length(rdata(1,:));
        for i=1:n        %it is initilization
            alfa(1,i)=b(i,rdata(1),1)*pi(i);
        end
        for t=1:(T-1)      %recurssion
            for j=1:n
                z=0;
                for i=1:n
                    z=z+a(i,j)*alfa(t,i);
                end
                for q=1:M
                    if rdata(t)==level(q)
                        alfa(t+1,j)=z*b(j,rdata(t+1),q);
                    end
                end
            end
        end
        p=0;
        for i=1:n         %termination
            p=p+alfa(T,i);
        end
        PAR1(1,r)=p;
    end
    
    for r=1:e
        rdata=data2(r,:);
        T=length(rdata(1,:));
        for i=1:n        %it is initilization
            alfa(1,i)=b2(i,rdata(1),1)*pi2(i);
        end
        for t=1:(T-1)      %recurssion
            for j=1:n
                z=0;
                for i=1:n
                    z=z+a2(i,j)*alfa(t,i);
                end
                for q=1:M
                    if rdata(t)==level(q)
                        alfa(t+1,j)=z*b2(j,rdata(t+1),q);
                    end
                end
            end
        end
        p=0;
        for i=1:n         %termination
            p=p+alfa(T,i);
        end
        p;
        PAR2(1,r)=p;
    end
    plr=0;
    for i=1:e
        plr=PAR12(1,i)+plr;
    end
    pfinal=plr;
    alf1=alfa1;
    alf2=alfa2;
    LOGP(qq,1)=pfinal;
end
for i=1:M
    emission(:,:,i)=b(:,:,i);
end

for i=1:M
    emission2(:,:,i)=b2(:,:,i);
end
emission
trans=a
pi
emission2
trans2=a2
pi2
LOGP
ALFA1=alf1
ALFA2=alf2
plot(1:1:iteration,LOGP,'r.')
end