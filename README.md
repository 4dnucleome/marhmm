%This program runs Autoregressive (ARHMM) and Mixture ARHMM (MARHHM). 


%In ARHMM, we have 5 inputs (data,level,a,pi,iteration)  and three outputs [trans,emission,LOGP]  

%data=Given observation sequence labellebd in numerics
%level=	Observation takes differnt values which will be denoted by level
%a=transition probability matrix
%pi=initial probability vector 
%iteration = number of iterations

%Open the ARHMM in matlab. 
%In command of Matlab program, please copy and paste the following entries:  
data=dlmread('real25.txt');
%data=dlmread('real50.txt');
%data=dlmread('real100.txt');
%data=dlmread('real240.txt');

level= [1 2 3];
a=[0.4,0.4,0.2;0.7,0.2,0.1;0.2,0.5,0.3]
pi=[0.3;0.4;0.3] 
iteration=500;

%Then  copy and paste:

 [trans,emission,LOGP]=ARHMM(data,level,a,pi,iteration)

%in command.

%Then you are asked to enter the dependent matrix b(:,:,i)  which defiens as P(Ot|st,O(t-1)=i). 
%please copy and paste the following matrix in front of b and then enter:

[0.5,0.2,0.3;0.4,0.1,0.5;0.2,0.7,0.1] 

%the same as previous step copy and paste the following matrix and enter

[0.5,0.2,0.3;0.4,0.1,0.5;0.2,0.7,0.1] 

%and finally copy and paste the following matrix and enter

[0.3,0.2,0.5;0.7,0.2,0.1;0.1,0.4,0.5] 

------------------------------------------------------

%In MARHMM we have 7 inputes (data,level,a,pi,iteration,alfa1,alfa2) in which alfa1,alfa2 are initial values of  mixture coefficients. 
%Please define the following equations in command:
alfa1=.5;
alfa2= .5;

%Then copy and Paste the  

[trans,emission,pi,trans2,emission2,pi2,LOGP,ALFA1,ALFA2]=MARHMM(data,level,a,pi,iteration,alfa1,alfa2)

%in the Matlab Command. 



 
