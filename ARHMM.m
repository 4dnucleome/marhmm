function [trans,emission,LOGP]=ARHMM(data,level,a,pi,iteration)
M=length(level(1,:));
for i=1:M
    b(:,:,i)=input('enter dependence matrix b :');
end
e=length(data(:,1));
T=length(data(1,:));
n=length(a(1,:));
LOGP=[];
PAR1=[];
hh=[];
yy=[];
vvv=[];
LLL=[];
Lq=[];
vq=[];
for qq=1:iteration
    logp=0;
    pi1=zeros(n,1);
    for i=1:M
        vqq(:,:,i)=zeros(n,M);
    end
    for i=1:M
        Lqq(:,:,i)=zeros(n,M);
    end
    zz=zeros(n,n);zzz=zeros(n,n);
    for r=1:e
        rdata=data(r,:);
        for i=1:n        %it is initilization
            m(1,i)=b(i,rdata(1),1)*pi(i);
            
        end
        for t=1:T-1      %recurssion
            
            for j=1:n
                z=0;
                for i=1:n
                    z=z+a(i,j)*m(t,i);
                end
                for q=1:M
                    if rdata(t)==level(q)
                        m(t+1,j)=z*b(j,rdata(t+1),q);
                    end
                end
            end
        end
        
        for i=1:n        %it is initilization
            d(T,i)=1;
        end
        for t=T-1:-1:1
            for i=1:n
                for q=1:M
                    if rdata(t)==level(q)
                        z=0;
                        for j=1:n;
                            z=z+a(i,j)*d(t+1,j)*b(j,rdata(t+1),q);
                        end
                        d(t,i)=z;
                    end
                end
            end
        end
        for t=1:T
            for i=1:n
                x=0;
                for j=1:n
                    x=x+(m(t,j)*d(t,j));
                end
                
                if x~=0
                    G(t,i)=(m(t,i)*d(t,i))/(x);
                end
            end
        end
        
        for t=1:T-1
            for i=1:n
                for j=1:n
                    for q=1:M
                        if rdata(t)==level(q)
                            w=0;
                            for k=1:n
                                w=w+(m(t,k)*d(t,k));
                            end
                            
                            if w~=0
                                E(i,j,t)=(m(t,i)*a(i,j)*b(j,rdata(t+1),q)*d(t+1,j))/(w);
                            end
                        end
                    end
                end
            end
        end
        pii=zeros(n,1);
        
        for i=1:n
            pii(i,1)=G(1,i);
        end
        
        for i=1:n
            for j=1:n
                h=0;y=0;
                for t=1:T-1
                    h=h+E(i,j,t);
                    y=y+G(t,i);
                end
                hh(i,j)=h;
                yy(i,j)=y;
            end
        end
        zz=zz+hh;
        zzz=zzz+yy;%  end%satr
        for q=1:M
            for j=1:n
                for k=1:M
                    L=0;
                    for t=2:T
                        if rdata(t-1)==level(q)
                            L=L+G(t,j);
                        end
                    end
                    LLL(j,k)=L;
                    v=0;
                    for  t=2:T
                        if rdata(t)==level(k) && rdata(t-1)==level(q)
                            v=v+G(t,j);
                        end
                    end
                    vvv(j,k)=v;
                end
            end
            vq(:,:,q)= vvv;
            Lq(:,:,q)= LLL;
            % bbb(:,:,q)
        end
        
        for i=1:M
            vqq(:,:,i)=vqq(:,:,i)+vq(:,:,i);
            Lqq(:,:,i)=Lqq(:,:,i)+Lq(:,:,i);
        end
        pi1=pi1+pii;
    end
    pi=(pi1/e);%end row
    for i=1:M
        b(:,:,i)=vqq(:,:,i)./Lqq(:,:,i);
    end
    a=zz./zzz;
    for r=1:e
        rdata=data(r,:);
        for i=1:n        %it is initilization
            alfa(1,i)=b(i,rdata(1),1)*pi(i);
        end
        for t=1:(T-1)      %recurssion
            for j=1:n
                z=0;
                for i=1:n
                    z=z+a(i,j)*alfa(t,i);
                end
                for q=1:M
                    if rdata(t)==level(q)
                        alfa(t+1,j)=z*b(j,rdata(t+1),q);
                    end
                end
            end
        end
        p=0;
        for i=1:n         %termination
            p=p+alfa(T,i);
        end
        p;
        pp=log10(p);
        PAR1(1,r)=pp;
    end
    for i=1:e
        logp=logp+PAR1(1,i);
    end
    
    LOGP(qq,1)=logp;
end
for i=1:M
    emission(:,:,i)=b(:,:,i);
end
emission
trans=a
pi
LOGP
plot(1:1:iteration,LOGP,'k.')
end